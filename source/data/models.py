from datetime import date
from pydantic import BaseModel


class Company(BaseModel):
    id: int | None
    name: str | None
    description: str | None
    logo: str | None

    @classmethod
    def from_query_result(cls, id, name, description, logo):
        return cls(
            id=id,
            name=name,
            description=description,
            logo = logo)


class Employee(BaseModel):
    id: int | None
    first_name: str
    last_name: str
    date_of_birth: date | None
    photo: str | None
    position: str | None
    salary: float | None
    company_id: int | None

    @classmethod
    def from_query_result(cls, id, first_name, last_name, date_of_birth, photo, position, salary, company_id):
        return cls(
            id=id,
            first_name=first_name,
            last_name=last_name,
            date_of_birth=date_of_birth,
            photo=photo,
            position=position,
            salary=salary,
            company_id=company_id
        )

