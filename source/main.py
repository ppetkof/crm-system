from fastapi import FastAPI
from routers.companies import companies_router
from routers.employee import employees_router


app = FastAPI()
app.include_router(companies_router)
app.include_router(employees_router)

