import unittest
from unittest.mock import patch
from datetime import date
from common.responses import BadRequest
from common import helpers
from data.models import Employee
from data import database
from mariadb import IntegrityError
from services.employee_service import create, delete, update_employee_info, get_employee_by_id, get_all



if __name__ == '__main__':
    unittest.main()