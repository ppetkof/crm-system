import json
import unittest

from fastapi.testclient import TestClient
from routers.employee import employees_router
from services import employee_service


client = TestClient(employees_router)

class TestEmployeeRouter(unittest.TestCase):

    def test_get_all_employees(self):
        response = client.get("/employees/")
        assert response.status_code == 200
        assert isinstance(response.json(), list)

    def test_get_employee_by_id(self):
        response = client.get("/employees/1")
        assert response.status_code == 200
        assert response.json()["id"] == 1

    def test_get_employee_by_id_with_bad_request(self):
        response = client.get("/employees/1000")
        assert response.status_code == 400


if __name__ == '__main__':
    unittest.main()