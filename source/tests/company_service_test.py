import unittest
from unittest.mock import Mock, create_autospec, patch
from common.responses import BadRequest
from data.models import Company, Employee
from services import company_service
from mariadb import IntegrityError

fake_company = Company(id=1,
                         name="Test name",
                         description = "Big company",
                         logo = "link",)





class CompanyServices_Should(unittest.TestCase):

    def test_create_company_when_data_isValid(self):
        #arrange
        company_service.helpers.upload_photo = Mock()
        company_service.helpers.upload_photo.return_value = "logo"
        insert_data_func = lambda query, arg2: 1

        #act
        result = company_service.create("Company Test", "Big company", "logo", insert_data_func)

        #assert
        self.assertEqual(Company, type(result))


    def test_create_with_integrity_error(self):
        # Arrange
        company_service.helpers.upload_photo = Mock()
        company_service.helpers.upload_photo.return_value = "logo"

        def insert_data_func(arg1, arg2):
            raise IntegrityError

        # Act
        result = company_service.create("Company Test", "Big company", "logo", insert_data_func)

        # Assert
        self.assertIsNone(result)

    def test_get_all_returns_all_companies(self):
        # Arrange
        get_data_func = lambda arg1: [
                (1, 'Test Com', 'Large corp', 'path/to/photo1'),
                (3, 'Test Com3', 'Mid corp', 'path/to/photo3')]

        # Act
        companies = list(company_service.get_all(search_by_name= None, get_data_func=get_data_func))

        # Assert
        self.assertEqual(len(companies), 2)
        self.assertEqual(companies[0].name, 'Test Com')
        self.assertEqual(companies[1].description, 'Mid corp')


    def test_get_all_filters_by_name(self):
        # Arrange
        get_data_func = lambda arg1: [
            (1, 'Test Com', 'Large corp', 'path/to/photo1'),
            (3, 'Test Enterprise', 'Mid corp', 'path/to/photo3')]

        # Act
        companies = list(company_service.get_all(search_by_name='Com', get_data_func=get_data_func))

        # Assert
        self.assertEqual(len(companies), 1)
        self.assertEqual(companies[0].name, 'Test Com')

    def test_get_company_by_id_when_IDisValid(self):
        # Arrange
        get_data_func= lambda arg1, arg2: [(fake_company.id, fake_company.name, fake_company.description, fake_company.logo)]
        # Act
        result = company_service.get_company_by_id(id=id, get_data_func=get_data_func)
        print(get_data_func)
        print(result)

        # Assert
        self.assertEqual(result, fake_company)

    def test_get_company_by_id_when_ID_NotValid(self):
        get_data_func = lambda arg1, arg2: []
        # Act
        result = company_service.get_company_by_id(id, get_data_func)
        # Assert
        self.assertIsNone(result)


    def test_delete_existing_company(self):
        # arrange
        company_id = 1
        company_service.get_company_by_id = Mock()
        company_service.get_company_by_id.return_value = fake_company
        expected_message = {"message": "Company deleted successfully."}
        del_data_func = lambda query, data: True

        # act
        result = company_service.delete(company_id, del_data_func)

        # assert
        self.assertEqual(result, expected_message)

    def test_delete_nonexistent_company(self):
        # arrange
        company_id = 100
        company_service.get_company_by_id = Mock()
        company_service.get_company_by_id.return_value = None
        del_data_func = lambda query, data: False

        # act
        result = company_service.delete(company_id, del_data_func)

        # assert
        self.assertEqual(type(result), BadRequest)

