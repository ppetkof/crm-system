from common import helpers
from common.responses import BadRequest
from data.database import delete_query, insert_query, read_query, update_query
from data.models import Company
from mariadb import IntegrityError


def create(name: str, description: str, logo: str,
           insert_data_func = None) -> Company | None:

    if insert_data_func is None:
        insert_data_func = insert_query #this function is used for isolation, and easy testing

    try:
        if logo is not None:
            upload_logo = helpers.upload_photo
            logo = upload_logo(logo, name)

        generated_id = insert_data_func(
            'INSERT INTO companies(name, description, logo) VALUES (?,?,?)',
            (name, description, logo))

        return Company(id=generated_id, name=name, description=description, logo=logo)

    except IntegrityError:

        return None

def get_all(search_by_name: str = None,  get_data_func=None):
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func(
        "SELECT c.id, c.name, c.description, c.logo FROM companies as c")

    result = (Company.from_query_result(*row) for row in data)

    if search_by_name is not None:
        return (company for company in result if search_by_name.lower() in company.name.lower())

    return result

def get_company_by_id(id, get_data_func = None) -> Company| None:
    if get_data_func is None:
        get_data_func = read_query
    data = get_data_func('SELECT * FROM companies WHERE id=?', (id,))
    return next((Company.from_query_result(*row) for row in data), None)


def delete(id: int, del_data_func=None):
    if del_data_func is None:
        del_data_func = delete_query

    if get_company_by_id(id) is not None:
        del_data_func("DELETE FROM companies WHERE id = ?", (id,))
        return {"message": "Company deleted successfully."}
    else:
        return BadRequest(f"No company with ID: {id}")

def update_company_info(id, new_company_info):

    company = get_company_by_id(id)
    if company is None:
        return None

    new_company_info.logo = helpers.upload_photo(new_company_info.logo or company.logo,
                                                company.name)

    company_merged = Company(id=company.id,
            name=company.name,
            description = new_company_info.description or company.description,
            logo = new_company_info.logo or company.logo)

    print(company_merged)
    update_query(
        '''UPDATE companies SET
            name = ?, description = ?, logo = ?
            WHERE id = ?
        ''',
        (company_merged.name, company_merged.description, company_merged.logo, company_merged.id))

    print(company_merged)
    return company_merged




