from common import helpers
from datetime import date
from common.responses import BadRequest
from data.database import delete_query, insert_query, read_query, update_query
from data.models import Employee
from mariadb import IntegrityError


def create(first_name: str,last_name: str, date_of_birth: date | None, photo: str | None,
           position: str | None, salary: float | None, company_id: int,
           insert_data_func = None) -> Employee | None:

    if insert_data_func is None:
        insert_data_func = insert_query

    try:
        if photo is not None:
            upload_photo = helpers.upload_photo
            photo = upload_photo(photo, last_name)

        generated_id = insert_data_func(
            'INSERT INTO employees(first_name, last_name, date_of_birth, photo, position, salary, company_id) VALUES (?,?,?,?,?,?,?)',
            (first_name, last_name, date_of_birth, photo, position, salary, company_id))

        return Employee(id=generated_id, first_name=first_name,last_name=last_name, date_of_birth=date_of_birth,photo=photo,
            position=position,salary=salary,company_id=company_id)

    except IntegrityError:
        return None


def get_all(search_by_name: str = None,  get_data_func=None):
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func(
        "SELECT id, first_name, last_name, date_of_birth, photo, position, salary, company_id FROM employees")

    result = (Employee.from_query_result(*row) for row in data)

    if search_by_name is not None:
        return (employee for employee in result if search_by_name.lower() in employee.first_name.lower() or search_by_name.lower() in employee.last_name.lower())

    return result

def get_employee_by_id(id, get_data_func = None) -> Employee| None:
    if get_data_func is None:
        get_data_func = read_query
    data = get_data_func('SELECT * FROM employees WHERE id=?', (id,))
    return next((Employee.from_query_result(*row) for row in data), None)


def delete(id: int, del_data_func=None):
    if del_data_func is None:
        del_data_func = delete_query

    if get_employee_by_id(id) is not None:
        del_data_func("DELETE FROM employees WHERE id = ?", (id,))
        return {"message": "Employee deleted successfully."}
    else:
        return BadRequest(f"No employee with ID: {id}")

def update_employee_info(id, updated_employee_info):

    employee = get_employee_by_id(id)
    if employee is None:
        return None

    updated_employee_info.photo = helpers.upload_photo(updated_employee_info.photo or employee.photo,
                                                employee.last_name)

    employee_merged = Employee(id=employee.id,
            first_name=employee.first_name,
            last_name=updated_employee_info.last_name or employee.last_name,
            date_of_birth=updated_employee_info.date_of_birth or employee.date_of_birth,
            photo=updated_employee_info.photo or employee.photo,
            position=updated_employee_info.position or employee.position,
            salary=updated_employee_info.salary or employee.salary,
            company_id=employee.company_id)

    update_query(
        '''UPDATE employees SET
            first_name=?, last_name=?, date_of_birth=?, photo=?, position=?, salary=? 
            WHERE id = ?
        ''',
        (employee_merged.first_name, employee_merged.last_name, employee_merged.date_of_birth, employee_merged.photo,
         employee_merged.position, employee_merged.salary, id))

    return employee_merged

