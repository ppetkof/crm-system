from fastapi import APIRouter, Header
from common.responses import BadRequest, Forbidden, NotFound, Success, Unauthorized
from data.models import Company, Employee
from services import company_service

companies_router = APIRouter(prefix='/companies')

@companies_router.post('/create', response_model=Company)
def create(company: Company):

    company = company_service.create(company.name, company.description, company.logo)

    if company is None:
        return BadRequest(f'Username is taken.')

    company_response = Company(id = company.id, name = company.name,  description=company.description,
                      logo=company.logo)

    return company_response


@companies_router.get('/', response_model=list[Company])
def get_companies(search_by_name: str | None = None,search_by_location: str | None = None):
    companies_list = company_service.get_all(search_by_name, search_by_location)
    return companies_list

@companies_router.get("/{company_id}")
def get_company(company_id: int):
    company = company_service.get_company_by_id(company_id)
    return company or BadRequest(f'Company with ID: {company_id} does not exist in the CRM database')

@companies_router.put('/{company_id}')
def update_info(company_id: int, new_company: Company | None):

    update_company_info = company_service.update_company_info(company_id, new_company)
    return update_company_info or BadRequest(f'Company with ID: {company_id} does not exist in the CRM database')

@companies_router.delete("/{company_id}")
def delete_company(company_id: int):
    result = company_service.delete(company_id)
    return result
