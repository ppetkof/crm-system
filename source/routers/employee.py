from fastapi import APIRouter, Header
from common.responses import BadRequest, Forbidden, NotFound, Success, Unauthorized
from data.models import Company, Employee
from services import employee_service, employee_service

employees_router = APIRouter(prefix='/employees')

@employees_router.post('/create')
def create(employee: Employee):

    employee = employee_service.create(employee.first_name, employee.last_name, employee.date_of_birth,
                                       employee.photo, employee.position, employee.salary, employee.company_id)
    print(employee)
    return employee or BadRequest(f'Please try again, filling the correct data')

@employees_router.get('/', response_model=list[Employee])
def get_companies(search_by_name: str | None = None,search_by_location: str | None = None):
    companies_list = employee_service.get_all(search_by_name, search_by_location)
    return companies_list

@employees_router.get("/{employee_id}")
def get_employee(employee_id: int):
    employee = employee_service.get_employee_by_id(employee_id)
    return employee or BadRequest(f'Company with ID: {employee_id} does not exist in the CRM database')

@employees_router.put('/{employee_id}')
def update_info(employee_id: int, new_employee: Employee | None):

    update_employee_info = employee_service.update_employee_info(employee_id, new_employee)
    return update_employee_info or BadRequest(f'Company with ID: {employee_id} does not exist in the CRM database')

@employees_router.delete("/{employee_id}")
def delete_employee(employee_id: int):
    result = employee_service.delete(employee_id)
    return result