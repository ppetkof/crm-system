# CRM system

Simple RESTFull API for CRUD operations with companies and employees

## About

The application has two main parts with very much alike structure: Comapnies and Employees;

Everyone can create, get the info, update and delete companies and employees 

```
## Set up

Technical Requirements
Python 3.10+
MariaDb
FastAPI
Uvicorn

Database
The project was developed on MariaDb using MySQL Workbech. Database file are provided in this repo

```

## How to run the project locally
Clone the repo.

Set up your database. You may import the database structure from dbfolder - simple_crm_structure_only.sql file. 
Project was developed using the Microsoft MySQL Workbench, but you can use a tool of your choice to import and manage the database.
Fill the database with data(You can use the provided simple_crm_data_only script from db folder.
Run the project with uvicorn main:app command. 

Check the swagger documentation at http://127.0.0.1:8000/docs for details about supported requests and valid format.
